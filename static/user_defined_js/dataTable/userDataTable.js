// # select by tag name
// # select by id (#)
// # select by class (.)
// document.getElementById()
// document.getElementsByClassName())
function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
const csrftoken = getCookie('csrftoken');
$(document).ready(function () {
    userDataTable = $('#user_table').DataTable({
        "paging":"false",
        "dom": 'lfrBtip',
        // "paging": "true",
        "ajax": {
            "url": userListUrl,
            "type": "GET",
            "dataSrc": "table_data",
            // "data": {"csrf_token": csrftoken}
        },
        columns: [
            {"title":"ID", "data": "id"},
            {"title":"username", "data": "username"}
        ],
        columnDefs: [
            {
                "targets": [1],
                "render": function (data, type, row, meta) {
                    return data.link('https://www.google.com')
                }
            }
        ]
    });
})

