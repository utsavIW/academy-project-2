$(document).ready(function () {
    $('button').prop("disabled", true);
})

$('#id_barcode').change(function () {
    var formData = {
        'barcode': $('#id_barcode').val(),
    }

    $.ajax({
        url: barcodeUrl,
        type: "GET",
        data: formData,
        data_type: 'json',
        success: function (result, status, xhr) {
            $('#id_unit').attr('type','text')
            $('#id_frequency').attr('type','text')
            $('button').prop("disabled", false);
        },
        error: function (result, status, xhr) {
            $('button').prop("disabled", true);
        }
    })
})
