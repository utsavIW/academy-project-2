var table;

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

$(document).ready(function () {
    table = $('#list_team_members').DataTable({
        "serverSide": true,
        "dom": 'T<"clear">lfrtip',
        "paging": "full_numbers",
        "ajax": {
            "url": table_url,
            "type": "POST",
            "data": {
                'csrfmiddlewaretoken': csrftoken
            }
        },
        "columns": [
            {"data": "name"},
            {"data": "position"},
            {"data": "experience"},
            {"data": "skills"},
            {"data": "description"},
            {"data": "image"},
            {
                "class": "text-right",
                "orderable": false,
                "data": null,
            },
        ],
        "columnDefs": [
            {
                "targets": [6],
                "render": function (data, type, row, meta) {
                    return '<a href="' + edit_url.replace(12345, data.id) + '" class="btn btn-icon-toggle" ' +
                        'data-toggle="tooltip" data-placement="top" data-original-title="Edit row">' +
                        '<i class="fa fa-pencil"></i></a>' +
                        '<button onclick="deleteMember(' + data.id + ')" class="btn btn-icon-toggle" ' +
                        'data-toggle="tooltip" data-placement="top" data-original-title="Delete row">' +
                        '<i class="fa fa-trash-o"></i></a>';
                }
            },
            {
                "targets": [5],
                "orderable": false,
                "render": function (data, type, row, meta) {
                    return '<img src="/media/' + data + '" height="100px" width= "100px">';
                }
            },
            {
                "targets": [1, 3, 4],
                "orderable": false
            },
        ],


        "order": [[0, 'asc']],
        "language": {
            "lengthMenu": '_MENU_ entries per page',
            "infoFiltered": "",
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        },
        "tableTools": {
            "aButtons": []
        }
    });


});

function deleteMember(id) {
    $.ajax({
        url: del_url.replace(12345, id),
        success: function (result, status, xhr) {
            if (result.success) {
                console.log(result)
                table.ajax.reload()
            }
        }
    })
}
