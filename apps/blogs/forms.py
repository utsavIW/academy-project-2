from django.forms import ModelForm

from apps.blogs.models import Blog


class CreateBlogForm(ModelForm):
    class Meta:
        model = Blog
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })
        # for visible in self.visible_fields():
        #     visible.field.widget.attrs['class'] = 'form-control'
        #     visible.field.widget.attrs['style'] = 'form-control'


