from django.shortcuts import render

# Create your views here.
from django.views.generic import CreateView

from apps.blogs.forms import CreateBlogForm
from apps.blogs.models import Blog


class BlogCreateView(CreateView):
    form_class = CreateBlogForm
    template_name = 'backend/creat_update/create.html'
    # model = Blog
    success_url = '/'
    # queryset = Blog.objects.all()
