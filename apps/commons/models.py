from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.
# from phonenumber_field.modelfields import PhoneNumberField

from apps.core.models import DateTimeModel

USER = get_user_model()


class Profile(DateTimeModel):
    image = models.ImageField(upload_to='user', blank=True)
    # phone_number = PhoneNumberField()
    user = models.OneToOneField(USER, on_delete=models.CASCADE, related_name='user_ko_profile')

    def __str__(self):
        return f"user {self.user} profile"

    @property
    def get_user_name(self):
        return self.user.username

    def check_user_is_super_user(self):
        return self.user.is_superuser


