from django.urls import path

from apps.core.views import *

urlpatterns = [
    path('login/',LoginView.as_view()),
    path('list_users/',user_datatable, name="list_users"),
]