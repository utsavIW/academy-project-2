# Create your views here.
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.views.generic import FormView
from apps.core.forms import LoginForm


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'authentication/uesr_login.html'

    def form_invalid(self, form):
        username = form.data.get('uesrname')
        pass

    def form_valid(self, form):
        from django.contrib.auth import authenticate

        username = form.data.get('username')
        password = form.data.get('password')

        print(username, password)
        user = authenticate(self.request, username=username, password=password)

        if user:
            login(self.request, user)
            return HttpResponseRedirect(self.request.META['HTTP_REFERER'])
        else:
            return HttpResponseRedirect(self.request.META['HTTP_REFERER'])


def user_datatable(request):
    if request.is_ajax():
        user = User.objects.all()
        user_json = list(user.values('id', 'username'))
        data = {'table_data':user_json}
        return JsonResponse(data, safe=False)
    return render(request, 'backend/datatable/user_datatable.html')
